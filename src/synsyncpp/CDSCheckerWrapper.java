package synsyncpp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.UUID;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;



public class CDSCheckerWrapper implements HttpSessionBindingListener {

	private String test_name;
	private String test_folder;
	private String test_orig_name;
	private String dot_file;
	private String dot_name;
	private String dot_location;
	private String cisout_file;
	private boolean needs_cleanup;
	
	public String getDot_file() {
		return dot_file;
	}
	public CDSCheckerWrapper(String test_name){
		this.test_name = this.test_orig_name = test_name;
		this.needs_cleanup = false;
		
	}

	
	public String runCis(String timeout_time) {
		
			cleanup();// just in case
			
		   String timeout = "timeout " + timeout_time;
		   String cis_location = GlobalDeclarations.cis_location;		
		   String executing_sh_script = "sh " +cis_location + "/run.sh";
		   test_folder  = cis_location + "/cis_benchmarks/" + test_name;
		   String test_todo = test_folder + "/" + test_name;
		   dot_name = "output.dot";
		   dot_location = test_folder;
		   dot_file = dot_location + "/" + dot_name;		   
		   String cis_params = "-m 2 -y 2 -d" + dot_file;
		   String exec_line = timeout + " " + executing_sh_script + " " + test_todo + " " + cis_params;
		   Executor executor = new Executor();
		   cisout_file = test_folder + "/" + test_name + ".out";
		   String cisout = executor.execute(exec_line);
		   try {
			writeCodeFile(cisout, cisout_file);
		} catch (FileNotFoundException e) {
			return "Couldn't create a cisout file named " + cisout_file + " for some reason! " + e.getMessage();
		}
		   return cisout;
	}
	public String runCis(String timeout_time, String code_text, UUID sessionId) {	
		
		   cleanup();// just in case

		   this.needs_cleanup = true;
		   this.test_name  = test_orig_name;
		   String timeout = "timeout " + timeout_time;
		   String cis_location = GlobalDeclarations.cis_location;		
		   String executing_sh_script = "sh " +cis_location + "/run.sh";
		   test_folder  = cis_location + "/cis_benchmarks/" + this.test_name + sessionId;
		   String test_orig_folder  = cis_location + "/cis_benchmarks/" + this.test_orig_name ;
		   
		   Executor mkdir_executor = new Executor();
		   String mkdir_command = "mkdir " + test_folder;
		   
		   String mkdir_output = mkdir_executor.execute(mkdir_command); 
		  if(! new File(test_folder).exists() ){
			  return "failed command\n" + mkdir_command + 
			   "\n Couldn't create a new dir named " + test_folder + " for some reason! \n Try checking GlobalDeclarations .java\n" + mkdir_output;
		  }

		   String test_todo = test_folder + "/" + test_name ;
		   
		   try {
			   writeCodeFile(code_text, test_todo + ".cc");
		   } catch (FileNotFoundException e) {
			return "Couldn't create a new file named " + test_todo + ".cc for some reason! " + e.getMessage();
		   }
		   
		   try {
			copyFilesAndSwitchStrings(new File(test_orig_folder + "/Makefile"),
					   new File(test_folder + "/Makefile"), test_orig_name, test_name);
		   } catch (IOException e) {
				return "Couldn't copy " + test_orig_folder + "/Makefile to " + test_folder + "/Makefile for some reason! " + e.getMessage();
		   }
		   /*
		   String command = "make" ; //the make command you wish to run
		   String [] envp = { } ;// if you want to set some environment variables
		   File dir = new File ( test_folder ) ;
		   Executor make_executor = new Executor();
		   String make_output = make_executor.execute(command, envp, dir); */

		   Executor make_executor = new Executor();
		   String command = "sh " + cis_location + "/synthesis/helping_scripts/cd_and_make.script " + test_folder ; //the make command you wish to run
		   String make_output = make_executor.execute(command); 
		   if (GlobalDeclarations.checkEmptyFile(test_todo)) {
			   return command + " failed. \n" +  make_output;
		   }
		   dot_name = "output.dot";
		   dot_location = test_folder;
		   dot_file = dot_location + "/" + dot_name;
		   String cis_params = "-m 2 -y 2 -d" + dot_file;
		   String exec_line = timeout + " " + executing_sh_script + " " + test_todo + " " + cis_params;
		   Executor executor = new Executor();
		   cisout_file = test_folder + "/" + test_name + ".out";
		   String cisout = executor.execute(exec_line);
		   try {
			writeCodeFile(cisout, cisout_file);
		} catch (FileNotFoundException e) {
			return "Couldn't create a cisout file named " + cisout_file + " for some reason! " + e.getMessage();
		}
		   return cisout;
	}
	public String getCisout_file() {
		return cisout_file;
	}
	private void writeCodeFile(String code_text, String test_todo)
			throws FileNotFoundException {
		PrintWriter out = new PrintWriter(test_todo);
		   out.println(code_text);
		   out.close();
	}
	public String getDot_name() {
		return dot_name;
	}
	public String getDot_location() {
		return dot_location;
	}
	public String runCis(){
		   String timeout = "5m";
		   return runCis(timeout);

	}
	
	
	private static void copyFilesAndSwitchStrings(File source, File dest, String old_str, String new_str)

	        throws IOException {

	    BufferedReader input = null;

        input = new BufferedReader(new  FileReader(source));   
	    String         line = null;
		PrintWriter out = new PrintWriter(dest);
	    
	    
	    while( ( line = input.readLine() ) != null ) {
	    	line = line.replaceAll(old_str,new_str);
	    	out.println(line);
	    }
	    
	    out.close();
	    input.close();
	}
	@Override
	public void valueBound(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		cleanup();
	}
	
	/**
	 * removes files and logs of previous CDSChecker run
	 * if needs_cleanup is true.
	 */
	public void cleanup(){
		
		if(this.needs_cleanup){		   
		   String command = "rm -rf " + test_folder;
		   Executor executor = new Executor();
		   executor.execute(command);
		}
		this.needs_cleanup = false;
	}
	
}
