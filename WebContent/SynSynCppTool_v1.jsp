<%@page import="java.util.regex.Pattern"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="sun.misc.Regexp"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.*"%>
<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Using GET Method to Read Form Data</title>
</head>
<body>
<center>
<h1>Using GET Method to Read Form Data</h1>
<ul>
<li><p><b>Script Name:</b>
   <%= request.getParameter("script_name")%>
</p></li>
<li><p><b>Executing:</b>
   <%
   // TODO get IP // or other way to make the call unique
   // create folder for IP
   String result;
   String test_name = request.getParameter("script_name"); 
   String cis_location = "/home/yurime/workspace/thesis/weak_memory_models/CDSChecker";		
   String executing_sh_script = "sh " +cis_location + "/run.sh";
   String test_folder  = cis_location + "/cis_benchmarks/" + test_name;
   String test_todo = test_folder + "/" + test_name;
   String dot_file = test_folder + "/" + "output.dot";
   String cis_params = "-m 2 -y 2 -d" + dot_file;
   String exec_line = executing_sh_script + " " + test_todo + " " + cis_params;
   	try {
			   Process p = Runtime.getRuntime().exec(exec_line);
			   InputStream stderr = p.getErrorStream();
	           InputStreamReader isr = new InputStreamReader(stderr);
	           InputStream stdin = p.getInputStream();
	           InputStreamReader isin = new InputStreamReader(stdin);
	           BufferedReader br = new BufferedReader(isr);
	           String line = null;
	           while ( (line = br.readLine()) != null){
  	   
	            }
	     	   p.waitFor();
			   result = "exit code: " + p.exitValue() + "\n";
			   
	 } catch (Exception e) {
	   result = e.getMessage()+ "\n";
	 }
		result += " done \n";
		%>
	<%=result%><br>
		Graph Name:  <select name="graph_name" id="sel" onchange="changeGraphPic()">
		<%
	//reading dot file 
	  //  BufferedReader br = new BufferedReader(new FileReader("output.txt"));
  	BufferedReader br = new BufferedReader(new FileReader(dot_file));
    String line = null;
    int graph_num = 0;
	    while ((line = br.readLine()) != null) {
	    	if(! line.contains("digraph") )continue;
	    	String[] parts = line.split(" ", 3);
	    	if(parts.length > 2){
	    		String graphName = parts[1] + "<br>";
	      		%>
   				<option value="<%=++graph_num%>"><%=graphName%></option>
	     	<%
	    	}
	   }
	   int num_graphs = graph_num;
		%>
		</select>
		   <%
	

		   br.close();
%>
<img src="graph_1.png" id="pict">
		<script>
function changeGraphPic() {
    var pict = document.getElementById("pict");
    var sel = document.getElementById("sel");
    var graph_num = sel.options[sel.selectedIndex].value;
    pict.src = "graph_" + graph_num + ".png";    
	
}
	</script>     
    
}
</p></li>

</ul>

</center>
</body>
</html>