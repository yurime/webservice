package synsyncpp;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ChangeGraph
 */
@WebServlet("/ChangeGraph_path")
public class ChangeGraph extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeGraph() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String graph_i = request.getParameter("graph_name");
		HttpSession session = request.getSession();
		String dot_place = (String)session.getAttribute("dotplace");
		String dotfile = (String)session.getAttribute("dotfile");
		
		
		String relativeWebPath = "/images";
		String absoluteDiskPath = getServletContext().getRealPath(relativeWebPath);
	    String png_file = absoluteDiskPath + "/graph_" + graph_i + ".png";
	    DisplayGraphFromDot dotdisp = new DisplayGraphFromDot(dotfile, png_file, graph_i);
	    String create_img_out = dotdisp.showGraph();
	    session.setAttribute("create_img_out", create_img_out);
		session.setAttribute("pngfile", "images" + "/graph_" + graph_i + ".png");
		session.setAttribute("selected_graph_id", graph_i);
	   RequestDispatcher dispatcher = request.getRequestDispatcher("displayCppPatterns.jsp");
	   dispatcher.forward(request, response);
	}

}
