<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.util.*"%>
<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Using GET Method to Read Form Data</title>
</head>
<body>
<center>
<h1>Using GET Method to Read Form Data</h1>
<ul>
<li><p><b>Script Name:</b>
   <%= request.getParameter("script_name")%>
</p></li>
<li><p><b>Executing:</b>
   <%
   String result;
   	try {
			   Process p = Runtime.getRuntime().exec("sh /home/yurime/workspace/thesis/weak_memory_models/CDSChecker/run.sh /home/yurime/workspace/thesis/weak_memory_models/CDSChecker/cis_benchmarks/loops/loop_t3 -d/home/yurime/workspace/thesis/weak_memory_models/CDSChecker/cis_benchmarks/loops/output.dot");
			   InputStream stderr = p.getErrorStream();
	           InputStreamReader isr = new InputStreamReader(stderr);
	           InputStream stdin = p.getInputStream();
	           InputStreamReader isin = new InputStreamReader(stdin);
	           BufferedReader br = new BufferedReader(isin);
	           String line = null;
	           %>
	     		<%="OUTPUT"%> <br><%
	            while ( (line = br.readLine()) != null){
	            	%>
	     		<%=line%> <br>
	     	<%
	            }
	     	%>
     		<%="OUTPUT"%> <br><%

     		
	           %>
	     		<%="ERROR"%> <br><%
	     			BufferedReader br2 = new BufferedReader(isr);
		            while ( (line = br2.readLine()) != null){
	            	%>
	     		<%=line%> <br>
	     	<%
	            }
	     	%>
     		<%="ERROR"%> <br><%
			   p.waitFor();
			   result = "exit code: " + p.exitValue() + "\n";
			   
			 } catch (Exception e) {
			   result = e.getMessage()+ "\n";
			 }
   			result += " done \n";

 		  //  BufferedReader br = new BufferedReader(new FileReader("output.txt"));
 		  	BufferedReader br = new BufferedReader(new FileReader("/home/yurime/workspace/thesis/weak_memory_models/CDSChecker/cis_benchmarks/loops/output.dot"));
		    String line = null;
 		    while ((line = br.readLine()) != null) {
            	%>
     		<%=line%> <br>
     	<%
 		   }
 		   br.close();
 
	%>
	<%=result%>
</p></li>

</ul>
</body>
</html>