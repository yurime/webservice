/*
 *
 * SB (Store Buffering)
 *
 * URL:
 *   http://www.justsoftwaresolutions.co.uk/threading/
 *
 */

//#include <atomic>
#include <threads.h>

#include "librace.h"
#include "impatomic.h"
#include <model-assert.h>
#include "mem_op_macros.h"

static std::atomic<int> x("x"), y("y");

uint32_t b = 0, a = 0;

void p0(void *arg)
{ 
    int t1_loop_itr_bnd = 1;
    int i_t1 = 0;
    while(++i_t1 <= t1_loop_itr_bnd){
        store(&x, 1, std::memory_order_relaxed);

        a = load(&y, std::memory_order_relaxed);
    }
}

void p1(void *arg)
{
    int t1_loop_itr_bnd = 1;
    int i_t2 = 0;
    while(++i_t2 <= t1_loop_itr_bnd){
        store(&y, 1, std::memory_order_relaxed);

        b = load(&x, std::memory_order_relaxed);
    }
}

int user_main(int argc, char **argv)
{
    thrd_t t1, t2;

    store(&x, 0, std::memory_order_seq_cst);
    store(&y, 0, std::memory_order_seq_cst);

    thrd_create(&t1, p0, NULL);
    thrd_create(&t2, p1, NULL);

    thrd_join(t1);
    thrd_join(t2);

    model_print("a = %d, b = %d \n", a, b);
    MODEL_ASSERT(!(a == 0 && b == 0));

    return 0;
}
