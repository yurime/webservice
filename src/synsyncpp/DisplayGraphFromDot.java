package synsyncpp;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class DisplayGraphFromDot  implements HttpSessionBindingListener {
	private String dot_file;
	private String png_file;
	private String graph_i;
	private boolean needs_cleanup = false;
	/**
	 * Creates a png file and returns run out
	 * @param dot_file
	 * @param png_file
	 * @param graph_i
	 * @return
	 */
	
	public DisplayGraphFromDot(String dot_file, String png_file, String graph_i) {
		this.dot_file = dot_file;
		this.png_file = png_file;
		this.graph_i = graph_i;
		this.needs_cleanup = false;
	}
	public String showGraph() {
			   String cis_location = GlobalDeclarations.cis_location;
			   String pngFromDotScript = cis_location + "/synthesis/helping_scripts/createPngFromDotWithMultGraphs.script"; 
			   String exec_line = pngFromDotScript + " " + dot_file + " " + png_file + " " + graph_i;
			   
			   
			   Executor executor = new Executor();
			   needs_cleanup = true;
			   return exec_line + "     \n" + executor.execute(exec_line);
		   }
	@Override
	public void valueBound(HttpSessionBindingEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		cleanup();
	}
	
	/**
	 * removes files and logs of previous CDSChecker run
	 * if needs_cleanup is true.
	 */
	public void cleanup(){
		
		if(this.needs_cleanup){		   
		   String command = "rm " + png_file;
		   Executor executor = new Executor();
		   executor.execute(command);
		}
		this.needs_cleanup = false;
	}
	}

