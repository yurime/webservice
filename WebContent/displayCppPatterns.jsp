<%@page import="synsyncpp.GlobalDeclarations"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="synsyncpp.DisplayGraphFromDot"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Result of running CDSChecker</title>
</head>


<script src="CodeMirror/lib/codemirror.js"></script>
<link rel="stylesheet" href="CodeMirror/lib/codemirror.css">
<script src="CodeMirror/mode/clike/clike.js"></script>



<body>



<jsp:useBean id="create_img_out" class="java.lang.String" scope="session">
</jsp:useBean>

<jsp:useBean id="dotfile" class="java.lang.String" scope="session"></jsp:useBean>
<jsp:useBean id="script_name" class="java.lang.String" scope="session"></jsp:useBean>

<jsp:useBean id="pngfile" class="java.lang.String" scope="session"></jsp:useBean>

<jsp:useBean id="code_text" class="java.lang.String" scope="session"></jsp:useBean>

<jsp:useBean id="selected_graph_id" class="java.lang.String" scope="session"></jsp:useBean>

<jsp:useBean id="cisout" class="java.lang.String" scope="session"></jsp:useBean>

<jsp:useBean id="cisout_file" class="java.lang.String" scope="session"></jsp:useBean>


<p id="cisoutDiv" style="display:none;"> 
Output: <br><textarea readonly> <%=cisout %> </textarea>
</p>
<p id="imgoutDiv" style="display:none;"> 
Output: <br><textarea readonly> <%=create_img_out %> </textarea>
</p>
<!--  Dot At: <%=dotfile %> <br> -->
<!--  Png At: <%=pngfile %> <br>-->
<div align="center">
<form action="SynSynCppTool_v1.htm">
    <input type="submit" value="Start Over">
</form>
</div>
<p>The graph</p>

<a href="http://www.cs.technion.ac.il/~yurime/PSynSyn_cameraready.pdf"> 
<span title="Shows graphs of C++ relaxed memory behaviour patterns observed in dekker's algorithm executions. 
Graph name meaning:
	 exec<num_execution><type of pattern><num_pattern>
	 type of pattern -- SB/LB/MP/WRC/RD1/RD2/RD1SC/RD2SC
	 num pattern -- number of pattern occurence in the same trace 
	                 (with different instructions[actions])
Graph node label: <tid>.<code line num> <instruction> (tid=1 is main)">

       <img src="StaticContent/questionMark.png" align="right" width=10pts>
</span>
</a>



<%
	if (GlobalDeclarations.checkEmptyFile(dotfile)){
		 %> Couldn't find:  <%=dotfile%> <% 
	}else{
		 %>

	<form action="ChangeGraph_path"  method="post">
		Graph Name:  <select name="graph_name" id="sel" onchange="this.form.submit()">
			<%
		//reading dot file 
		  //  BufferedReader br = new BufferedReader(new FileReader("output.txt"));
			
	  	BufferedReader br = new BufferedReader(new FileReader(dotfile));
	    String line = null;
	    int graph_num = 0;
	    while ((line = br.readLine()) != null) {
	    	if(! line.contains("digraph") )continue;
	    	String[] parts = line.split(" ", 3);
	    	if(parts.length > 2){
	    		String graphName = parts[1] + "<br>";
	      		%>
   				<option value="<%=++graph_num%>"
   						<% if (selected_graph_id.equalsIgnoreCase(""+graph_num)){ %> selected="selected" <% } %>   				  				
   				><%=graphName%>
   				</option>
	     	<%
	    	}
	   }
	   int num_graphs = graph_num;
		%>
		</select>
	   <%


	   br.close();
	}
%>
</form>


    
    
<button onclick="showOutput()" id="showButton">show CDSChecker output</button>
<button onclick="hideOutput()" id="hideButton" style="display:none;">hide CDSChecker output</button>

<button onclick="showImgOutput()" id="showImgButton">show Image creation output</button>
<button onclick="hideImgOutput()" id="hideImgButton" style="display:none;">hide Image creation output</button>

<script>
function showOutput() {
    
	document.getElementById("cisoutDiv").style="";
	document.getElementById("hideButton").style="";
	document.getElementById("showButton").style="display:none;";

}
</script>
    
   <script>
function hideOutput() {
	document.getElementById("cisoutDiv").style="display:none;";
	document.getElementById("hideButton").style="display:none;";
	document.getElementById("showButton").style="";

}
</script>
   
<script>
function showImgOutput() {
    
	document.getElementById("imgoutDiv").style="";
	document.getElementById("hideImgButton").style="";
	document.getElementById("showImgButton").style="display:none;";

}
</script>
    
   <script>
function hideImgOutput() {
	document.getElementById("imgoutDiv").style="display:none;";
	document.getElementById("hideImgButton").style="display:none;";
	document.getElementById("showImgButton").style="";

}
</script>

<img src=<%=pngfile%> id="pict">

   <div id="cisoutDiv"></div>

 <textarea id="cpp-code" ><%=code_text%></textarea>
    
<script>
var myCodeMirror =  CodeMirror.fromTextArea(document.getElementById("cpp-code"),{
		        lineNumbers: true,
		        matchBrackets: true,
	  mode:  "text/x-c++src",
	  readOnly: true
	});
myCodeMirror.setSize(1000,700);
</script>    

</body>
</html>