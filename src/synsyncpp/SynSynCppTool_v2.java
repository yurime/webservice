package synsyncpp;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;


/**
 * Servlet implementation class SynSynCppTool_v2
 * Controller:
 * Gets script name from user
 * creates an object which will execute the script and return the path of the result
 * then a viewer will display it.
 * The viewer will generate an image each time
 */
@WebServlet(description = "second attempt at creating the server side", urlPatterns = { "/SynSynCppTool_v2_path" })
public class SynSynCppTool_v2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SynSynCppTool_v2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		   HttpSession session = request.getSession();
		   UUID sessionId = UUID.randomUUID();
		   session.setAttribute("sessionID", sessionId);
		   
		   String test_name = request.getParameter("script_name");
		   String code_text = request.getParameter("code_text");
		   boolean changed = Boolean.parseBoolean(request.getParameter("changed"));
		   CDSCheckerWrapper cis = new CDSCheckerWrapper(test_name);
		   String timeout = "5m";
		   String cisout = null;
		   if( ! changed ) {
			   RequestDispatcher dispatcher = request.getRequestDispatcher("StaticContent/"+ test_name + "/" + test_name + "Graphs.html");
			   dispatcher.forward(request, response);
			   return;
		   }else{
			   cisout = cis.runCis(timeout, code_text, sessionId);
		   }
		   String dot_place = cis.getDot_location();
		   String dot_name = cis.getDot_name();
		   String dot_file = cis.getDot_file();
		   if (GlobalDeclarations.checkEmptyFile(dot_file)){
			   PrintWriter out = response.getWriter();
			   out.print(cisout);
			   out.print("Can't find " +  dot_file);
			   out.print("The run had an error (perhaps timout) within " +timeout + " run");
		   }else{		   			   
			   session.setAttribute("dotfile", dot_file);		   
			   session.setAttribute("cisout", cisout);		   
			   session.setAttribute("cisout_file", cis.getCisout_file());
			   session.setAttribute("dotplace", dot_place);
			   session.setAttribute("dotname", dot_name);
			   session.setAttribute("script_name", test_name);
			   session.setAttribute("code_text", code_text);
			   session.setAttribute("cis", cis);
		   		
			   RequestDispatcher dispatcher = request.getRequestDispatcher("selectCppPatterns.jsp");
			   dispatcher.forward(request, response);
		   }
		   /*
		   ServletOutputStream out = response.getOutputStream();
		   out.print("changed is: " + changed);
		   out.println("request.getParameter(\"changed\") is: " + request.getParameter("changed") );
		   */
		   return;
		   
	}


	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Auto-generated method stub
	}
*/
}
