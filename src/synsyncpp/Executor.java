package synsyncpp;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public class Executor {

	public String execute(String command) {

	    StringBuffer output = new StringBuffer();

	    Process p;
	    try {
	        p = Runtime.getRuntime().exec(command);
	        p.waitFor();
	        BufferedReader reader = 
	                        new BufferedReader(new InputStreamReader(p.getInputStream()));

	        String line = "";
	        output.append("Output: \n");
	        while ((line = reader.readLine())!= null) {
	            output.append(line + "\n");
	        }
	        BufferedReader err_reader = 
                    new BufferedReader(new InputStreamReader(p.getErrorStream()));
	        String err_line = "";
	        output.append("Error: \n");
	        while ((err_line = err_reader.readLine())!= null) {
	            output.append(err_line + "\n");
	        }

	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    return output.toString();

	}
	
	public String execute(String command, String[] envp, File dir) {

	    StringBuffer output = new StringBuffer();

	    Process p;
	    try {
	        p = Runtime.getRuntime().exec(command, envp, dir);
	        p.waitFor();
	        BufferedReader reader = 
	                        new BufferedReader(new InputStreamReader(p.getInputStream()));

	        String line = "";
	        output.append("Output: \n");
	        while ((line = reader.readLine())!= null) {
	            output.append(line + "\n");
	        }
	        BufferedReader err_reader = 
                    new BufferedReader(new InputStreamReader(p.getErrorStream()));
	        String err_line = "";
	        output.append("Error: \n");
	        while ((err_line = err_reader.readLine())!= null) {
	            output.append(err_line + "\n");
	        }

	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    return output.toString();

	}
	
}
