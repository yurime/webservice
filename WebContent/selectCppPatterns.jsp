<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="synsyncpp.DisplayGraphFromDot"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Result of running CDSChecker</title>
</head>
<body>

<jsp:useBean id="cisout_file" class="java.lang.String" scope="session"></jsp:useBean>

<jsp:useBean id="cisout" class="java.lang.String" scope="session"></jsp:useBean>

<jsp:useBean id="dotfile" class="java.lang.String" scope="session"></jsp:useBean>

<p id="cisoutDiv" style="display:none;"> 
Output:<br><textarea readonly> <%=cisout %> </textarea>
</p>
<!--  Dot At: <%=dotfile %> <br> -->
<div align="center">
<form action="SynSynCppTool_v1.htm">
    <input type="submit" value="Start Over">
</form>
</div>
<p>The graph</p>

<a href="http://www.cs.technion.ac.il/~yurime/PSynSyn_cameraready.pdf"> 
<span title="Shows graphs of C++ relaxed memory behaviour patterns observed in dekker's algorithm executions. 
Graph name meaning:
	 exec<num_execution><type of pattern><num_pattern>
	 type of pattern -- SB/LB/MP/WRC/RD1/RD2/RD1SC/RD2SC
	 num pattern -- number of pattern occurence in the same trace 
	                 (with different instructions[actions])
Graph node label: <tid>.<code line num> <instruction> (tid=1 is main)">

       <img src="StaticContent/questionMark.png" align="right" width=10pts>
</span>
</a>

<form action="ChangeGraph_path"  method="post">
	Graph Name:  <select name="graph_name" id="sel" onchange="this.form.submit()">
	  <option selected="selected" style="display:none;">Select Graph</option>
		<%
	//reading dot file 
	  //  BufferedReader br = new BufferedReader(new FileReader("output.txt"));
  	BufferedReader br = new BufferedReader(new FileReader(dotfile));
    String line = null;
    int graph_num = 0;
	    while ((line = br.readLine()) != null) {
	    	if(! line.contains("digraph") )continue;
	    	String[] parts = line.split(" ", 3);
	    	if(parts.length > 2){
	    		String graphName = parts[1] + "<br>";
	      		%>
   				<option value="<%=++graph_num%>"><%=graphName%></option>
	     	<%
	    	}
	   }
	   int num_graphs = graph_num;
		%>
		</select>
		   <%
	

		   br.close();
%>
</form>
    
    
<button onclick="showOutput()" id="showButton">show CDSChecker output</button>
<button onclick="hideOutput()" id="hideButton" style="display:none;">hide CDSChecker output</button>

<script>
function showOutput() {
    
	document.getElementById("cisoutDiv").style="";
	document.getElementById("hideButton").style="";
	document.getElementById("showButton").style="display:none;";

}
</script>
    
   <script>
function hideOutput() {
	document.getElementById("cisoutDiv").style="display:none;";
	document.getElementById("hideButton").style="display:none;";
	document.getElementById("showButton").style="";

}
</script>
    


   

</body>
</html>