package synsyncpp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GlobalDeclarations {

	//static public String cis_location = "/home/yurime/workspace/thesis/weak_memory_models/CDSChecker";
	static public String cis_location = "/workspace/cdscheckersynthesis";
	
	static public String execute2(String exec_line){
		String result = "";
	   	try {
				   Process p = Runtime.getRuntime().exec(exec_line);
				   InputStream stderr = p.getErrorStream();
		           InputStreamReader isr = new InputStreamReader(stderr);
		           InputStream stdin = p.getInputStream();
		           InputStreamReader isin = new InputStreamReader(stdin);
		           BufferedReader br = new BufferedReader(isr);
		           BufferedReader brin = new BufferedReader(isin);
		           String line = null;
		           result += "\n Erors\n";
		           while ( (line = br.readLine()) != null){
		        	   result += line;
		            }
		           result += "\n Output\n";
		           while ( (line = brin.readLine()) != null){
		        	   result += line;
		            }
		     	   p.waitFor();
				   result = "exit code: " + p.exitValue() + "\n";
				   //br.close();				   
		 } catch (Exception e) {
		   result = e.getMessage()+ "\n";
		 }
		result += " done \n";
	   	return result;
	}
	
	/**
	 * Tries to read the given file. <br>
	 * returns true if a non empty line was read <br>
	 * return false if NullPointerException,  FileNotFoundException, or IOException is thrown.
	 * @param file_path
	 * @return
	 */
	static public boolean checkEmptyFile(String file_path){
		BufferedReader br;
		boolean result = false;
		try {
			br = new BufferedReader(new FileReader(file_path));     
			result = (br.readLine() == null);
			br.close();
		} catch (FileNotFoundException e) {
			result = true;
		} catch (NullPointerException e) {
			result = true;
		}catch (IOException e) {
			result = true;
		}
		return result;
		
	}

}
